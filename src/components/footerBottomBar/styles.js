
/**
 * Auto generated component for OneySelfCare
 * Author: Elza
 * Date: 5/5/2020
 */

import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        height: 50,
        paddingVertical:15,
        paddingHorizontal: 25,
        backgroundColor: '#CCCCCC',
        flexDirection:"row",
        justifyContent:"space-between",
    },
    text: {
        fontSize: 12
    }
});