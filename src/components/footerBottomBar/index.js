import React from "react";
import { View, Text } from "react-native";
import { buildComponents } from "../../mappings/componentsBuilder";
import styles from "./styles";
    
export default ({ navigation, props: { text, components} }) => {
        
    return (
        <View style={styles.container}>
            <Text style={styles.text}>{text}</Text>
            <View>{buildComponents(navigation, components)}</View>
        </View>
    );
};
